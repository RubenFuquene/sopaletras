<?php

use model\GestorPalabras;

if (isset($_POST["palabraNueva"]))
{
    $gestorPalabras = new GestorPalabras();
    $gestorPalabras->almacenarPalabra($_POST["palabraNueva"]);
}