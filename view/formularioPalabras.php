<?php
	//evalua si hay una palabra para cargar e incrusta los scripts necesarios para realizar la carga
	if (isset($_POST["palabraNueva"]))
	{
		include '../entities/Palabra.php';
		include '../model/Random.php';
		include '../model/constants.php';
    	include '../model/GestorPalabras.php';		
    	include '../controller/controlador.php';
	}else
	{
    	$_SESSION["palabras"] = null;
	}
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo (isset($_POST['palabraNueva']) ? '../' : ''); ?>css/bootstrap.min.css">
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="<?php echo (isset($_POST['palabraNueva']) ? '../' : ''); ?>js/bootstrap.min.js"></script>

    <title>Sopa de letras (Capturador de palabras)</title>
  </head>
  <body>
    <div class="jumbotron">
	  <h1 class="display-4">Sopa de letras</h1>
	  <p class="lead">Empezaremos por capturar las palabras que estarán en la sopa de letras</p>
	  <hr class="my-4">
	  <p>
		<div class="row">
			<div class="col">
			  <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#formularioCargaPalabra" aria-expanded="false" aria-controls="multiCollapseExample2">Cargar palabra</button>
			</div>
			<div class="col">
			  <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#palabrasCargadas" aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2">Ver palabras cargadas</button>
			</div>
		</div>
	  <p>
	
		<div class="row">
		  <div class="col">
		    <div class="collapse multi-collapse" id="formularioCargaPalabra">
		      <div class="card card-body">
		        <form method="post" action="<?php echo (isset($_POST['palabraNueva']) ? '../' : ''); ?>view/formularioPalabras.php" onsubmit="return validaciones()">
		          <div class="form-group">
				    <label for="exampleInputEmail1">Palabra a agregar</label>
				    <input type="text" class="form-control" id="palabraNueva" name="palabraNueva" placeholder="Nueva Palabra" onkeyup="javascript:this.value=this.value.toUpperCase();">
				    <small class="form-text text-muted">La palabra solo deberá tener caracteres alfabéticos; sin acentos, espacios ni caracteres especiales.</small>
				  </div>
				  
		          <button type="submit" class="btn btn-primary">Cargar</button>
		        </form>
		        <p>
		        <div class="alert alert-danger collapse" role="alert" id="alertaCaracteres">
  					La palabra <strong>NO</strong> debe contener acentos, ni caracteres especiales, ni espacios
				</div>
				<div class="alert alert-danger collapse" role="alert" id="alertaCampoVacio">
  					Introduzca una palabra, por favor.
				</div>
				</p>
		      </div>
		    </div>
		  </div>
		  <div class="col">
		    <div class="collapse multi-collapse" id="palabrasCargadas">
		      <div class="card card-body">
			    <ul class="list-group">
					<?php
					// muestra las palabras cargadas
					if (isset($_SESSION["palabras"]))
   					{
        				foreach ($_SESSION["palabras"] as $cadaPalabra)
						{
							if(!is_int($cadaPalabra))
							{
								echo '<li class="list-group-item">' . $cadaPalabra->getValor() . '</li>';
							}
						}
    				}		
					?>
				</ul>
		      </div>
		    </div>
		  </div>
		</div>
	  <hr class="my-4">
	  <form action="sopaLetras.php">
	  	<div class="form-check">
	  		<input type="checkbox" class="form-check-input" id="colorearPalabras" name="colorearPalabras">
    		<label class="form-check-label" for="exampleCheck1">active la palomita si desea colorear las palabras</label>
    	</div>
	  	<button type="submit" <?php echo (isset($_SESSION["palabras"]) ? "" : "disabled"); ?> class="btn btn-success">Crear sopa de letras</button>
	  </form>
	</div>
	<script type="text/javascript" src="<?php echo (isset($_POST['palabraNueva']) ? '../' : ''); ?>js/adicional.js"></script>	
  </body>
</html>