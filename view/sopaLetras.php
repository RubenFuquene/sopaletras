<?php
include '../model/Random.php';
include '../model/constants.php';
include '../model/GestorSopa.php';
include '../entities/Palabra.php';

use model\Random;
use model\GestorSopa;

//inicio de sesi�n de PHP
session_start();

//El tama�o de la sopa ser� el tama�o de la palabra mas larga multiplicado por 2
$cFilasColumnas = $_SESSION["palabras"]["masGrande"]  * 2 < 10 ? 10 : $_SESSION["palabras"]["masGrande"]  * 2;
//construccion de la sopa de letras
if (!isset($_GET["mantenerRejilla"]))
{
    $gestorSopa = new GestorSopa($cFilasColumnas, $_SESSION["palabras"]);
    $_SESSION["sopaLista"] = $gestorSopa->crearSopa();
}

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <title>Sopa de letras (Capturador de palabras)</title>
  </head>
  <body>

  	<a class="btn btn-primary" href="sopaLetras.php?mantenerRejilla=on<?php echo (isset($_GET['colorearPalabras']) ? '' : '&colorearPalabras=on'); ?>"  role="button">Colorear / descolorear las palabras</a>
  	<a class="btn btn-primary" href="../index.php" role="button">Volver al principio</a>
  	<hr class="my-4">
  	<table class="table table-bordered">
  	 <tbody>
  	 <?php
  	 
  	 //armado de la rejilla de letras con la sopa ya creada
  	 for ($cadaFila = 0; $cadaFila < $cFilasColumnas; $cadaFila++)
  	 {
  	     echo "<tr>";
  	     for ($cadaCol = 0; $cadaCol < $cFilasColumnas; $cadaCol++)
  	         {
  	             echo "<td ";
  	             if (isset($_SESSION["sopaLista"][$cadaFila][$cadaCol]))
  	             {
  	                 if (isset($_GET["colorearPalabras"]))
  	                 {
  	                     echo "class='table-success' >";
  	                 }
  	                 else
  	                     echo ">";
  	                 
  	                     echo $_SESSION["sopaLista"][$cadaFila][$cadaCol];
  	             }else
  	             {
  	                 echo ">" . Random::letraRandom();
  	             }
  	             
  	             echo "</td>";
  	         }
  	     echo "</tr>";
  	     
  	 }
  	 ?>
     </tbody>
    </table>
  </body>
</html>