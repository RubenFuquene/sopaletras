<?php
// constantes de direcci�n
define("DIRECCION_1", "norte");
define("DIRECCION_2", "sur");
define("DIRECCION_3", "este");
define("DIRECCION_4", "oeste");
define("DIRECCION_5", "noreste");
define("DIRECCION_6", "noroeste");
define("DIRECCION_7", "sureste");
define("DIRECCION_8", "suroeste");

//Letras disponibles dentro de la rejilla
$letras [1] = "A";
$letras [2] = "B";
$letras [3] = "C";
$letras [4] = "D";
$letras [5] = "E";
$letras [6] = "F";
$letras [7] = "G";
$letras [8] = "H";
$letras [9] = "I";
$letras [10] = "J";
$letras [11] = "K";
$letras [12] = "L";
$letras [13] = "M";
$letras [14] = "N";
$letras [15] = "O";
$letras [16] = "P";
$letras [17] = "Q";
$letras [18] = "R";
$letras [19] = "S";
$letras [20] = "T";
$letras [21] = "U";
$letras [22] = "V";
$letras [23] = "W";
$letras [24] = "X";
$letras [25] = "Y";
$letras [26] = "Z";
$letras [27] = "&Ntilde;";
