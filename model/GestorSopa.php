<?php
namespace model;

use entities\Palabra;

/**
 * Clase encargada de la gesti�n de la rejilla de letras.
 * 
 * @author ruben
 *
 */
class GestorSopa
{
    private $tamagno;
    private $palabras;
    private $sopa;
    
    /**
     * 
     * @param int $tamagno tama�o cuadrado de la rejilla
     * @param array $palabras array de 'entities\Palabra' para cargar en la rejilla.
     */
    function __construct(int $tamagno, array $palabras)
    {
        $this->tamagno = $tamagno;
        $this->palabras = $palabras;
        $this->sopa = array();
    }
    
    /**
     * funci�n encargada de la creacion de la sopa
     * 
     * @return array array de caracteres.
     */
    public function crearSopa()
    {
        $palabraInsertada = true;
        
        foreach ($this->palabras as $cadaPalabra)
        {
            if (!is_int($cadaPalabra))
            {
                do
                {
                    $palabraInsertada = $this->asignarPosiciones($cadaPalabra);
                }while (!$palabraInsertada);
                
                $this->agnadirASopa($cadaPalabra);
            }
        }
        
        return $this->sopa;
    }
    
    /**
     * funci�n encargada de dar posicion a una palabra dentro de la rejilla.
     * 
     * @param Palabra $palabra palabra a posicionar
     * @return boolean resultado de la asignacion (si fue exitosa o no)
     */
    private function asignarPosiciones(Palabra $palabra)
    {
        $this->asignarPosicionInicial($palabra);
        
        $operacionCorrecta = true;
        $filaInicio = $palabra->getPosicion()["fila"][0];
        $colInicio = $palabra->getPosicion()["columna"][0];

        if (isset($this->sopa[$filaInicio][$colInicio]) && $this->sopa[$filaInicio][$colInicio] != substr($palabra->getValor(), 0, 1))
        {
            $operacionCorrecta = false;
        }
        else
        {            
            for ($cadaLetra = 1; $cadaLetra < $palabra->getTamagno(); $cadaLetra++)
            {
                switch ($palabra->getSentido())
                {
                    case constant("DIRECCION_1"):
                        $filaInicio--;
                                                
                        if (isset($this->sopa[$filaInicio][$colInicio]) && $this->sopa[$filaInicio][$colInicio] != substr($palabra->getValor(), $cadaLetra, 1))
                        {
                            $operacionCorrecta = false;
                            $cadaLetra = $palabra->getTamagno();
                        }else
                            $palabra->setPosSigLetra($filaInicio, $colInicio);
                    break;
                            
                    case constant("DIRECCION_2"):
                        $filaInicio++;
                        
                        if (isset($this->sopa[$filaInicio][$colInicio]) && $this->sopa[$filaInicio][$colInicio] != substr($palabra->getValor(), $cadaLetra, 1))
                        {
                            $operacionCorrecta = false;
                            $cadaLetra = $palabra->getTamagno();
                        }else
                            $palabra->setPosSigLetra($filaInicio, $colInicio);
                    break;
                            
                    case constant("DIRECCION_3"):
                        $colInicio++;
                        
                        if (isset($this->sopa[$filaInicio][$colInicio]) && $this->sopa[$filaInicio][$colInicio] != substr($palabra->getValor(), $cadaLetra, 1))
                        {
                            $operacionCorrecta = false;
                            $cadaLetra = $palabra->getTamagno();
                        }else
                            $palabra->setPosSigLetra($filaInicio, $colInicio);
                    break;
                            
                    case constant("DIRECCION_4"):
                        $colInicio--;
                        
                        if (isset($this->sopa[$filaInicio][$colInicio]) && $this->sopa[$filaInicio][$colInicio] != substr($palabra->getValor(), $cadaLetra, 1))
                        {
                            $operacionCorrecta = false;
                            $cadaLetra = $palabra->getTamagno();
                        }else
                            $palabra->setPosSigLetra($filaInicio, $colInicio);
                    break;
                            
                    case constant("DIRECCION_5"):
                        $filaInicio--;
                        $colInicio++;
                        
                        if (isset($this->sopa[$filaInicio][$colInicio]) && $this->sopa[$filaInicio][$colInicio] != substr($palabra->getValor(), $cadaLetra, 1))
                        {
                            $operacionCorrecta = false;
                            $cadaLetra = $palabra->getTamagno();
                        }else
                            $palabra->setPosSigLetra($filaInicio, $colInicio);
                    break;
                            
                    case constant("DIRECCION_6"):
                        $filaInicio--;
                        $colInicio--;
                        
                        if (isset($this->sopa[$filaInicio][$colInicio]) && $this->sopa[$filaInicio][$colInicio] != substr($palabra->getValor(), $cadaLetra, 1))
                        {
                            $operacionCorrecta = false;
                            $cadaLetra = $palabra->getTamagno();
                        }else
                            $palabra->setPosSigLetra($filaInicio, $colInicio);
                    break;
                            
                    case constant("DIRECCION_7"):
                        $filaInicio++;
                        $colInicio++;
                                                
                        if (isset($this->sopa[$filaInicio][$colInicio]) && $this->sopa[$filaInicio][$colInicio] != substr($palabra->getValor(), $cadaLetra, 1))
                        {
                            $operacionCorrecta = false;
                            $cadaLetra = $palabra->getTamagno();
                        }else
                            $palabra->setPosSigLetra($filaInicio, $colInicio);
                    break;
                            
                    case constant("DIRECCION_8"):
                        $filaInicio++;
                        $colInicio--;                        
                        
                        if (isset($this->sopa[$filaInicio][$colInicio]) && $this->sopa[$filaInicio][$colInicio] != substr($palabra->getValor(), $cadaLetra, 1))
                        {
                            $operacionCorrecta = false;
                            $cadaLetra = $palabra->getTamagno();
                        }else
                            $palabra->setPosSigLetra($filaInicio, $colInicio);
                    break;
                }
            }
        }
        
        return $operacionCorrecta;
    }
    
    /**
     * funci�n encargada de incrustar una palabra previamente posicionada dentro de la rejilla.
     * 
     * @param Palabra $palabra palabra previamente posicionada dentro de la rejilla.
     */
    private function agnadirASopa(Palabra $palabra)
    {
        for ($cadaLetra = 0; $cadaLetra < $palabra->getTamagno(); $cadaLetra++)
        {
            $fila = $palabra->getPosicion()["fila"][$cadaLetra];
            $columna = $palabra->getPosicion()["columna"][$cadaLetra];
            
            $this->sopa[$fila][$columna] = substr($palabra->getValor(), $cadaLetra, 1);
        }
    }
    
    /**
     * funci�n encargada de darle punto de inicio a una palabra dentro de la rejilla.
     * 
     * @param Palabra $palabra palabra a posicionar
     */
    private function asignarPosicionInicial(Palabra $palabra)
    {        
        $palabra->reiniciarPosicion();
        
        switch ($palabra->getSentido())
        {
            case constant("DIRECCION_1"): //norte
                $palabra->setPosicionInicial(rand($palabra->getTamagno() - 1, $this->tamagno - 1), rand(0, $this->tamagno - 1));
                break;
                
            case constant("DIRECCION_2")://sur
                $palabra->setPosicionInicial(rand(0, ($this->tamagno - $palabra->getTamagno()) - 1), rand(0, $this->tamagno - 1));
                break;
                
            case constant("DIRECCION_3")://este
                $palabra->setPosicionInicial(rand(0, $this->tamagno - 1), rand(0, ($this->tamagno - $palabra->getTamagno()) - 1));
                break;
                
            case constant("DIRECCION_4")://oeste
                $palabra->setPosicionInicial(rand(0, $this->tamagno - 1), rand($palabra->getTamagno() - 1, $this->tamagno - 1));
                break;
                
            case constant("DIRECCION_5")://noreste
                $palabra->setPosicionInicial(rand($palabra->getTamagno() - 1, $this->tamagno - 1), rand(0, ($this->tamagno - $palabra->getTamagno()) - 1));
                break;
                
            case constant("DIRECCION_6")://noroeste
                $palabra->setPosicionInicial(rand($palabra->getTamagno() - 1, $this->tamagno - 1), rand($palabra->getTamagno() - 1, $this->tamagno - 1));
                break;
                
            case constant("DIRECCION_7")://sureste
                $palabra->setPosicionInicial(rand(0, ($this->tamagno - $palabra->getTamagno()) - 1), rand(0, ($this->tamagno - $palabra->getTamagno()) - 1));
                break;
                
            case constant("DIRECCION_8")://suroeste
                $palabra->setPosicionInicial(rand(0, ($this->tamagno - $palabra->getTamagno()) - 1), rand($palabra->getTamagno() - 1, $this->tamagno - 1));
                break;
        }
    }
}