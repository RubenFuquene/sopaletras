<?php
namespace model;

/**
 * Clase para generar datos aleatorios
 * 
 * @author ruben
 *
 */
class Random
{
    /**
     * 
     * @return String direccion en la que se ubicar� la palabra
     */
    public static function obtenerDireccion()
    {
        return constant("DIRECCION_" . rand(1, 8));
    }
    
    /**
     * retorna una letra aleatoria.
     * 
     * @return String letra aleatoria.
     */
    public static function letraRandom()
    {
        return $GLOBALS["letras"][rand(1, 27)];
    }
}