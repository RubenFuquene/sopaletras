<?php
namespace model;

use entities\Palabra;

/**
 * Clase encargada de gestionar las palabras que el cliente quiere que salgan en la rejilla
 * 
 * @author ruben
 *
 */
class GestorPalabras
{
    /**
     * Almacena una 'entities\Palabra' en la variable de sesi�n para su uso durantet toda la ejecuci�n de la APP.
     * 
     * @param String $nuevaPalabra string a cargar como nueva palabra de la rejilla
     */
    public function almacenarPalabra(String $nuevaPalabra)
    {
        //inicio de sesi�n de PHP
        session_start();
        
        if (!isset($_SESSION["palabras"]))
        {
            $_SESSION["palabras"] = array();
            $_SESSION["palabras"]["masGrande"] = 0;
        }
        
        $_SESSION["palabras"][$nuevaPalabra] = new Palabra();
        $_SESSION["palabras"][$nuevaPalabra]->setValor($nuevaPalabra);
        $_SESSION["palabras"][$nuevaPalabra]->setTamagno(strlen($nuevaPalabra));
        $_SESSION["palabras"][$nuevaPalabra]->setSentido(Random::obtenerDireccion());
        $_SESSION["palabras"]["masGrande"] = ($_SESSION["palabras"]["masGrande"] < strlen($nuevaPalabra) ? strlen($nuevaPalabra) : $_SESSION["palabras"]["masGrande"]);
    }
}