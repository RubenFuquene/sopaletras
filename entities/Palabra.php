<?php
namespace entities;

/**
 * Clase entidad para el envolvimiento de lo que representa una palabra dentro de la rejilla de letras.
 * 
 * @author ruben
 *
 */
class Palabra
{
    private $valor;
    private $posicion;
    private $tamagno;
    private $sentido;
    
    /**
     * Clase entidad para el envolvimiento de lo que representa una palabra dentro de la rejilla de letras.
     */
    function __construct() 
    {
        $this->posicion["fila"][] = 0;
        $this->posicion["columna"][] = 0;
    }
    
    /**
     * Retorna la palabra
     * 
     * @return String la palabra
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * retorna la posici�n de todos los caracteres de la palabra dentro de la rejilla.
     * 
     * @return array posici�n organizados por filas y columnas
     */
    public function getPosicion()
    {
        return $this->posicion;
    }

    /**
     * retorna el tama�o de la palabra
     * 
     * @return int tama�o de la palabra
     */
    public function getTamagno()
    {
        return $this->tamagno;
    }

    /**
     * retorna el sentido que tendr� la palabra en la rejilla
     * 
     * @return String el sentido especificado en 'model/constants' 
     */
    public function getSentido()
    {
        return $this->sentido;
    }

    /**
     * setea la palabra
     * 
     * @param String $valor la palabra
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    /**
     * carga la posici�n del primer caracter de la palabra dentro de la rejilla
     * 
     * @param int $fila
     * @param int $columna
     */
    public function setPosicionInicial($fila, $columna)
    {
        $this->posicion["fila"][0] = $fila;
        $this->posicion["columna"][0] = $columna;
    }
    
    /**
     * carga una posici�n para un caracter de la palabra
     * 
     * @param int $fila
     * @param int $columna
     */
    public function setPosSigLetra($fila, $columna)
    {
        $this->posicion["fila"][] = $fila;
        $this->posicion["columna"][] = $columna;
    }

    /**
     * setea el tama�o de la palabra
     * 
     * @param int $tamagno el tama�o
     */
    public function setTamagno($tamagno)
    {
        $this->tamagno = $tamagno;
    }

    /**
     * setea el sentido que tendr� la palabra en la rejilla
     * 
     * @param String $sentido el sentido especificado en 'model/constants' 
     */
    public function setSentido($sentido)
    {
        $this->sentido = $sentido;
    }
    
    /**
     * borra la posici�n asignada a la palabra dentro de la rejilla
     */
    public function reiniciarPosicion()
    {
        $this->posicion = array();
    }
}