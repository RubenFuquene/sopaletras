/**
 * valida que la palabra a agregar se pueda poner en la rejilla
 * 
 * @returns booolean resultado de la validación
 */
function validaciones()
{
	var filtroLetras=/^[A-Za-z]+$/;
	
	if(!document.getElementById("palabraNueva").value.trim())
	{
		document.getElementById("alertaCampoVacio").className = "alert alert-danger";
		document.getElementById("alertaCaracteres").className = "collapse";
		return false;
	}else if(!filtroLetras.test(document.getElementById("palabraNueva").value.trim()))
	{
		document.getElementById("alertaCaracteres").className = "alert alert-danger";
		document.getElementById("alertaCampoVacio").className = "collapse";
		return false;
	}
	
	return true;
}